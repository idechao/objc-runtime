//
//  Car.h
//  MsgSend
//
//  Created by mfw on 15/6/19.
//  Copyright (c) 2015年 MFW. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Car : NSObject

- (void)run;

@end
