//
//  NSObject+MyClass.m
//  block
//
//  Created by rdlm on 15/2/3.
//  Copyright (c) 2015年 joke_smileZhang. All rights reserved.
//

#import "NSObject+MyClass.h"
#import <objc/runtime.h>


@implementation NSObject (MyClass)

+ (void)exChangeClassMethod:(Class)class Selector:(SEL)selector WithOtherSelector:(SEL)otherSelector
{
    Method otherMethod = class_getClassMethod(class, selector);
    Method method = class_getClassMethod(class, otherSelector);
    method_exchangeImplementations(otherMethod, method);
}
+ (void)exChangeInstanceMethod:(Class)class Selector:(SEL)selector WithOtherSelector:(SEL)otherSelector
{
    Method otherMethod = class_getInstanceMethod(class, selector);
    Method method = class_getInstanceMethod(class, otherSelector);
    method_exchangeImplementations(otherMethod, method);
}
+ (IMP)swizzleSelector:(SEL)origSelector
                withIMP:(IMP)newIMP {
    Class class = [self class];
    Method origMethod = class_getInstanceMethod(class,
                                                origSelector);
    IMP origIMP = method_getImplementation(origMethod);
    if(!class_addMethod(self, origSelector, newIMP,
                        method_getTypeEncoding(origMethod)))
    {
        method_setImplementation(origMethod, newIMP);
    }
    return origIMP;  
}
@end

@implementation NSMutableArray (MyClass)

+(void)load
{
    [self exChangeInstanceMethod:NSClassFromString(@"__NSArrayM") Selector:@selector(addObject:) WithOtherSelector:@selector(addMyObject:)];
}

- (void)addMyObject:(id)anObject
{
    if(!anObject)
    {
        NSLog(@"传入空值了");
        return;
    }
    [self addMyObject:anObject];
}
- (id)myObjectForKey:(id)aKey
{
    if(![self isKindOfClass:[NSMutableArray class]]){
        return nil ;
    }
    return [self myObjectForKey:aKey];
}

@end

@implementation NSMutableDictionary  (MyClass)

+(void)load
{
   [self exChangeInstanceMethod:NSClassFromString(@"__NSDictionaryM") Selector:@selector(setObject:forKey:) WithOtherSelector:@selector(setMyObject:forKey:)];
}
- (void)setMyObject:(id)anObject forKey:(id<NSCopying>)aKey
{
    if(!anObject)
    {
        NSLog(@"传入空值了,为%@",aKey);
        return;
    }
    [self setMyObject:anObject forKey:aKey];
}

@end

@implementation NSArray (MyClass)

+(void)load
{
    [self exChangeInstanceMethod:NSClassFromString(@"__NSArrayI") Selector:@selector(myObjectAtIndex:) WithOtherSelector:@selector(objectAtIndex:)];
}
- (id)myObjectAtIndex:(NSUInteger)index
{
    if(index >= self.count)
    {
        return nil;
    }
    return [self myObjectAtIndex:index];
}

@end
