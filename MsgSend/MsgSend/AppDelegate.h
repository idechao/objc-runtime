//
//  AppDelegate.h
//  MsgSend
//
//  Created by mfw on 15/3/11.
//  Copyright (c) 2015年 MFW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

