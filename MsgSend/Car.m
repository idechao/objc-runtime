//
//  Car.m
//  MsgSend
//
//  Created by mfw on 15/6/19.
//  Copyright (c) 2015年 MFW. All rights reserved.
//

#import "Car.h"
#import "Person.h"

@implementation Car

//- (void)run {
//    NSLog(@"%s %s %d", __func__, sel_getName(_cmd), __LINE__);
//}

//1 让你自己为这个方法增加实现
+ (BOOL)resolveInstanceMethod:(SEL)sel {
    return [super resolveInstanceMethod:sel];
}

//2 返回你需要转发消息的对象
- (id)forwardingTargetForSelector:(SEL)aSelector {  //首先调用的时候是无法重载run方法的时候
    return [[Person alloc] init];
}

//3
- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {
    NSString *sel = NSStringFromSelector(aSelector);
    if ([sel isEqualToString:@"run"]) {
        return [NSMethodSignature signatureWithObjCTypes:"v@:"];    //v代表返回值为void，@表示self，:表示_cmd
    }
    return [super methodSignatureForSelector:aSelector];
}

- (void)forwardInvocation:(NSInvocation *)anInvocation {
    SEL selector = [anInvocation selector];
    Person *person = [[Person alloc] init];
    if ([person respondsToSelector:selector]) {
        [anInvocation invokeWithTarget:person];
    }
}

@end
