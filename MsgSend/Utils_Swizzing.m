//
//  Utils_Swizzing.m
//  MsgSend
//
//  Created by mfw on 15/3/12.
//  Copyright (c) 2015年 MFW. All rights reserved.
//

#import "Utils_Swizzing.h"

//使用时可直接：
//objc_changeMethod(self, @selector(setBackgroundImage:forBarPosition:barMetrics:), setBackgroundImageBarPosition);

IMP objc_changeInstanceMethod(Class cls, SEL sel, IMP newIMP) {
    Method originMethod = class_getInstanceMethod(cls, sel);
    IMP originIMP = method_getImplementation(originMethod);
    if (!class_addMethod(cls, sel, newIMP, method_getTypeEncoding(originMethod))) {
        method_setImplementation(originMethod, newIMP);
    }
    return originIMP;
}

IMP objc_changeClassMethod(Class cls, SEL sel, IMP newIMP) {
    Method originMethod = class_getInstanceMethod(cls, sel);
    IMP originIMP = method_getImplementation(originMethod);
    Class metaClass = objc_getMetaClass(object_getClassName(cls));
    if (!class_addMethod(metaClass, sel, newIMP, method_getTypeEncoding(originMethod))) {
        method_setImplementation(originMethod, newIMP);
    }
    return originIMP;
}

void objc_changeInstanceMethodWithSelector(Class cls, SEL origigSEL, SEL newSEL) {
    method_exchangeImplementations(class_getInstanceMethod(cls, origigSEL), class_getInstanceMethod(cls, newSEL));
}

void objc_changeClassMethodWithSelector(Class cls, SEL originSEL, SEL newSEL) {
    method_exchangeImplementations(class_getClassMethod(cls, originSEL), class_getClassMethod(cls, newSEL));
}
