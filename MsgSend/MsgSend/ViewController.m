//
//  ViewController.m
//  MsgSend
//
//  Created by mfw on 15/3/11.
//  Copyright (c) 2015年 MFW. All rights reserved.
//

#import "ViewController.h"
#import <objc/message.h>
#import "Second.h"
#import "Utils_Swizzing.h"
#import "NSObject+MyClass.h"
#import "Person.h"
#import "Car.h"

NSInteger getDdata(NSInteger index) {
    NSLog(@"%ld", index);
    return index;
}

@interface ViewController ()

- (NSArray *)getPrivateAPI:(Class)cls;

@end

@implementation ViewController

+ (void) load {
    //建议写在load里
    objc_changeInstanceMethodWithSelector(self.class, @selector(getAge), @selector(getData));
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //消息转发
    [self messageTransmit];
    
#if 0
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    
    CGRect (*sendRectFn)(id receiver, SEL operation);
//    sendRectFn = (CGRect(*)(id, SEL))objc_msgSend_stret;
//    __unused CGRect frame = sendRectFn(view, @selector(frame));
    
//    CGRect cc = ((CGRect(*)(id, SEL))objc_msgSend)(view, @selector(frame));
    
#pragma mark - objc_msgsend
    Second *second = [[Second alloc] init];
    
    __unused NSString *ss = ((NSString *(*)(id, SEL))objc_msgSend)(second, @selector(getMyName));
    
    NSString *name = ((NSString *(*)(id, SEL, NSInteger))objc_msgSend)(self, @selector(getMyNames:),10);
    NSLog(@"%@", name);
    
    NSString *name2 = ((NSString *(*)(id, SEL, NSInteger))objc_msgSend)(self, NSSelectorFromString(@"getMyNames:"),12);
    NSLog(@"%@", name2);
    
#pragma makr - testSwizzing
    //先判空，之后会调用loolupclass
    Class cc =  objc_getClass("ViewController");
    NSLog(@"%@",cc);
    Class ccc = objc_lookUpClass("ViewControllers");
    NSLog(@"%@", ccc);
    //若找不到，在crash
    id dd = objc_getRequiredClass("ViewController");
    NSLog(@"%@", dd);
//    Class ddd = objc_getFutureClass("ViewController");
//    NSLog(@"%@", ddd);
    
    // 获取已注册的类定义的列表
    int bb1 = objc_getClassList(nil, 0);
    NSLog(@"%d", bb1);
    
    int versio = class_getVersion(self.class);
    NSLog(@"%d", versio);
    
    NSLog(@"-----------------");
#pragma mark - swizzing
    
    objc_changeInstanceMethod(self.class, @selector(objectAtIndex:), (void *)getDdata(5));
    objc_changeClassMethod(self.class, @selector(arrayWithObject:), (void *)getDdata(10));
    
    NSMutableArray *array = nil;
    [array addObject:nil];
    
    [self getData];
    
    NSLog(@"%@",[NSArray class]);
    NSLog(@"%@", [[NSArray array] class]);
    NSLog(@"%@", [[NSMutableArray array] class]);
    
#pragma mark - objc_setAssociatedObject 关联
    static char associatedKey;
    NSArray *arr = [[NSArray alloc] initWithObjects:@"one", @"two", @"three", nil];
    NSString *overview = [[NSString alloc] initWithFormat:@"%@",@"First three numbers"];
    objc_setAssociatedObject(arr, &associatedKey, overview, OBJC_ASSOCIATION_RETAIN);
    
    NSLog(@"关联==%@", objc_getAssociatedObject(arr, &associatedKey));
    

    
#pragma mark - test "NSObject+MyClass.h"
    NSArray *arr2 = [[NSArray alloc] initWithObjects:@"1", @"11",@"15",@"14",@"31",@"3",@"2", nil];
    NSLog(@"%@", [arr2 objectAtIndex:6]);
    NSLog(@"%@", [arr objectAtIndex:7]);
    
    //打印私有API
    NSLog(@"API = %@", [self getPrivateAPI:[UIView class]]);
#endif
}

#pragma makr - 消息转发
//简书：http://www.jianshu.com/p/1bde36ad9938
- (void)messageTransmit {
    Person *p = [[Person alloc] init];
    [p run];
    
    Car *car = [[Car alloc] init];
    [car run];
}



#pragma mark - 获取私有API
- (NSArray *)getPrivateAPI:(Class)cls {
    NSMutableArray *nameArr = [[NSMutableArray alloc] init];
    const char *cClassName = [NSStringFromClass(cls) UTF8String];
    id theClass = objc_getClass(cClassName);
    unsigned int outCount = 0;
    Method *m = class_copyMethodList(theClass, &outCount);
    NSLog(@"the method's count = %d", outCount);
    for (int i = 0; i < outCount; i++) {
        SEL tmpSEL = method_getName(*(m+i));
        NSString *sn = NSStringFromSelector(tmpSEL);
        [nameArr addObject:sn];
    }
    return (NSArray *)nameArr;
}

- (NSString *)getMyNames:(NSInteger) age {
    if (age > 10) return @"ming";
    else return @"xiao";
}

- (void)getData {
    NSLog(@"-0-0-0-getData");
//    return index;
}

-(void)getAge {
    NSLog(@"=0=0=0=0=getAge");
}




@end
