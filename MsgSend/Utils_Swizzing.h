//
//  Utils_Swizzing.h
//  MsgSend
//
//  Created by mfw on 15/3/12.
//  Copyright (c) 2015年 MFW. All rights reserved.
//

#import <objc/runtime.h>

//更改函数基础函数
//返回旧函数指针
IMP objc_changeInstanceMethod(Class cls, SEL sel, IMP newIMP);

IMP objc_changeClassMethod(Class cls, SEL sel, IMP newIMP);

void objc_changeInstanceMethodWithSelector(Class cls, SEL origigSEL, SEL newSEL);

void objc_changeClassMethodWithSelector(Class cls, SEL originSEL, SEL newSEL);
