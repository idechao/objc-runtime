//
//  Person.m
//  MsgSend
//
//  Created by mfw on 15/6/18.
//  Copyright (c) 2015年 MFW. All rights reserved.
//

#import "Person.h"
#import <objc/runtime.h>

void onFoot(id self, SEL _cmd) {
    NSLog(@"%@ %s %d", self, sel_getName(_cmd), __LINE__);
}

@implementation Person

//- (void)run {
//    NSLog(@"%@  %s", NSStringFromSelector(_cmd), sel_getName(_cmd));
//}

+ (BOOL)resolveInstanceMethod:(SEL)sel {
    if (sel == @selector(run)) {
        class_addMethod(self, sel, (IMP)onFoot, "v@:");
        return YES;
    }
    return [super resolveInstanceMethod:sel];
}

@end
