//
//  Person.h
//  MsgSend
//
//  Created by mfw on 15/6/18.
//  Copyright (c) 2015年 MFW. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

- (void)run;

@end
