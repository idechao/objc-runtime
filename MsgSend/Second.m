//
//  Second.m
//  MsgSend
//
//  Created by mfw on 15/3/11.
//  Copyright (c) 2015年 MFW. All rights reserved.
//

#import "Second.h"
#import <objc/runtime.h>

void functionForMethod1(id self, SEL _cmd) {
    NSLog(@"%@, %p", self, _cmd);
}

@implementation Second

- (NSString *)getMyName {
    return @"this is my name!";
}

/**
 *  动态方法解析
 *  这种方案更多的是为了实现@dynamic属性。
 *
 *  @param sel 
 *
 *  @return BOOL
 */
+ (BOOL)resolveInstanceMethod:(SEL)sel {
    NSString *selectorString = NSStringFromSelector(sel);
    
    if ([selectorString isEqualToString:@"method1"]) {
        class_addMethod(self.class, @selector(method1),  (IMP)functionForMethod1, "@:");
    }
    return [super resolveInstanceMethod:sel];
}

/**
 *  备用接收者
 *  如果一个对象实现了这个方法，并返回一个非nil的结果，则这个对象会作为消息的新接收者，且消息会被分发到这个对象
 *  这个对象不能是self自身，否则就是出现无限循环
 *
 *  @param aSelector
 *
 *  @return id
 */
- (id)forwardingTargetForSelector:(SEL)aSelector {
    return nil;
}

@end
