//
//  ViewController.h
//  MsgSend
//
//  Created by mfw on 15/3/11.
//  Copyright (c) 2015年 MFW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (NSString *)getMyNames:(NSInteger) age;

- (void)getAge;
- (void)getData;



@end

